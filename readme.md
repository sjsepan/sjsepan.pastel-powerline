# sjsepan.pastel-powerline TOML theme for Bash using Starship and NerdFonts on Linux Mint. Also tested on fish and PowerShell on Linux, and fish and bash on GhostBSd.

Customization based on pastel-powerline preset
Edited using VSCode and Better TOML extension

![sjsepan.pastel-powerline.png](./sjsepan.pastel-powerline.png?raw=true "Screenshot")

## Starship home

<https://starship.rs/>

## NerdFonts home

<https://www.nerdfonts.com/>

## TOML Home

Tom's Obvious Minimal Language

<https://toml.io/en/>

## Install Starship

`curl -O https://starship.rs/install.sh`

`chmod +x install.sh`

`mkdir ~/.local/share/applications/starship`

`./install.sh -b ~/.local/share/applications/starship`

`export PATH="~/.local/share/applications/starship:$PATH"`

Then execute (see 'Execute')

## Use my pastel-powerline .toml

Copy starship.toml to ~/.config

Go To 'Start automatically' instructions

## Create .toml from Preset

`starship preset --list`

`starship preset pastel-powerline > ~/.config/starship.toml`

Go To 'Start automatically' instructions

## Start automatically (Linux)

Edit the appropriate profile for each shell to be used (see 'Edit').

Add this or equivalent line to end of profile for default shell:

`export PATH="~/.local/share/applications/starship:$PATH"`

Then add the line to execute (see 'Execute')

## Customize

Make changes to ~/.config/starship.toml; to test re-execute (see 'Execute')

## Edit 

`xed ~/.bashrc`

or

`xed ~/.config/fish/fish.config`

or

`xed ~/.config/powershell/Microsoft.PowerShell_profile.ps1`

## Execute

`eval "$(starship init bash)"`

or

`starship init fish | source`

or

`Invoke-Expression (&starship init powershell)`

## NerdFonts Cheat-Sheet

<https://www.nerdfonts.com/cheat-sheet>

## NerdFix

<https://github.com/loichyan/nerdfix>

## History

1.5:

~ fix formatting for Git Staged count

1.4:

~Note: as of the most recent GhostBSD updates, specifically for NerdFonts (v3) and Starship (v1.18.2), the work-around .toml file is no longer needed, as the symbols in the standard .toml file appear normally.

1.3:

~Fix indicator when using 'screen' command: alter env_var section w/ alternate symtax using STY variable, and display session id next to symbol

~Reorganize readme from ad-hoc collection of notes to more of a guide.

1.2:

~change style on tools to darken foreground on light background

~replace NodeJS symbol, which was not showing in cool-retro-term

1.1:

~Update standard .TOML file to reflect live changes that work on both Linux and UNIX; additional changes for UNIX only are presented as comments

~Add freebsd-x64 compatible .TOML file with additional changes needed to work on FreeBSD or GhostBSD (due to NerdFont pkg available there). Rename to 'starship.toml on target system.

1.0:

~apply suggested changes from NerdFix

0.9:

~add support for starship 1.15.0 and the NerdFonts 3.0 which it supports; handle broken clock ($time) icon due to NF3 changes

0.8:

~Add shell indicator support, and define for Bash, Fish, PowerShell and unknown

~Change FG color of time to white for contrast

~Define OS indicator support

0.7:

~Use env_var with $STY to add support for Linux 'screen' by displaying symbol in prompt when active.

0.6:

~Add support for SSH by displaying remote hostname in prompt when connected.

0.5:

~Modify format of prompt to narrow the previous command status area.

0.4:

~Modify format of prompt to show dotnet symbol in blue FG and text in black FG.

0.3:

~Modify format of prompt to prefix short bar before with Status. Check-mark on success, X and error message on failure.

0.2:

~modify format of prompt to include continuation-prompt

~change user icon and move to front

~add schema for vscode completions

~add format and display of dotnet module

~replaced symbol for music, documents, downloads, pictures folders

~replaced symbol for time

0.1:

~initial release of theme

## Issues

~dotnet $version is recently not showing on my linux installation

## Contact

Stephen J Sepan

<sjsepan@yahoo.com>

7-13-2024
